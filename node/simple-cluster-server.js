const cluster = require('cluster');
const http = require('http');
const numCPUs = require('os').cpus().length;
const url = require('url');
const fs = require('fs');
const path = require('path');
const port = 9000

if (cluster.isMaster) {
    console.log(`Master ${process.pid} is running`);
    // Fork workers.
    for (var i = 0; i < numCPUs; i++) {
        cluster.fork();
    }

    cluster.on('exit', (worker, code, signal) => {
        console.log(`worker ${worker.process.pid} died`);
    });

} else {

    http.createServer((req, res) => {

        if (req.method === 'POST') {
            var body = '';

            req.on('data', function(data) {
                body += data;
                if (body.length > 1e6) {
                    req.connection.destroy();
                }
            });

            req.on('end', function() {
                var url_parts = url.parse(req.url, true);
                var time = new Date().toISOString();
                console.log(`[${time}] :: Request on worker ${process.pid} :: (${req.headers.host}) [${req.method}] -- ${req.url} -- ${url_parts.query.nodeId} -- ${url_parts.pathname}`);

                if (url_parts.pathname === '/add' || url_parts.pathname === '/put') {
                    fs.writeFileSync(url_parts.query.nodeId, body);
                    res.writeHead(201);
                    res.end('ok\n');
                } else if (url_parts.pathname === '/get') {
                    var r = fs.readFileSync(path.resolve(__dirname, url_parts.query.nodeId));
                    res.writeHead(200);
                    res.end(r);
                } else if (url_parts.pathname === '/delete') {
                    res.writeHead(200);
                    res.end('ok\n');
                } else {
                    res.writeHead(404);
                    res.end('not ok');
                }

                console.log(`[${time}] :: Response from worker ${process.pid} :: (${req.headers.host}) [${req.method}] -- ${req.url}`);
            });
        }

    }).listen(port);

    console.log(`Worker ${process.pid} started, listening on Port ${port}`);
}
