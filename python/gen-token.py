import base64
import sys
import requests, json

if len(sys.argv) != 3:
	print "Usage: gen-token.py <app_id> <app_secret>"
	exit

app_id = sys.argv[1]
app_secret = sys.argv[2]
encoded = base64.urlsafe_b64encode("%s:%s" %(app_id, app_secret))

url = 'https://sso.factern.com/auth/token'
payload = 'grant_type=client_credentials'
headers = {'content-type': 'application/x-www-form-urlencoded', 'Accept': 'application/json', 'Authorization': 'Basic ' + encoded}
r = requests.post(url, data=payload, headers=headers)

print r.text

exit
