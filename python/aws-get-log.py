#!/usr/local/bin

#
# For downloading logs from CloudWatch. Ex.
#
#     python aws_get_log.py Factern-Watches-LogGroup-KZ5YWNWSUO09 e835b3d0d9b028036dcdbd070a37fb3b5ebb0c34d0c21e2842840af19c953c40 --start 1476728429000 --end 1476729629000 --profile prod --region eu-west-1
#

import argparse
from datetime import datetime
import json
import os

desc = "Download logs from CloudWatch."

parser = argparse.ArgumentParser(description=desc)

parser.add_argument("log",
                    metavar="log",
                    nargs='+',
                    help="Ex. DataService or more verbosely Factern-Watches-LogGroup-KZ5YWNWSUO09 e835b3d0d9b028036dcdbd070a37fb3b5ebb0c34d0c21e2842840af19c953c40")

parser.add_argument("-s",
                    "--start",
                    help="the start time in milliseconds since epoch (defaults to one hour ago)")

parser.add_argument("-p",
                    "--profile",
                    help="Aws profile ex. prod")

parser.add_argument("-r",
                    "--region",
                    help="Aws region ex. eu-west-1")

parser.add_argument("-e",
                    "--end",
                    help="the end time in milliseconds since epoch (default to now)")

parser.add_argument("-m",
                    "--most-recent",
                    help="Number of most recent logs to download. Defaults to most recent log")

args = parser.parse_args()

current_time_utc = int((datetime.utcnow() - datetime(1970, 1, 1)).total_seconds() * 1000)

start = int(args.start or (current_time_utc - 3600 * 1000))

stop = int(args.end or str(current_time_utc))

most_recent = int(args.most_recent or 1)

if len(args.log) < 1 or len(args.log) > 2:
    raise Exception("Must specify one log-group-name and optionally a log-stream-name")

log_group_name = args.log[0]
log_stream_name = args.log[1] if len(args.log) == 2 else None
profile = args.profile or "prod"
region = args.region or "eu-west-1"

output = os.popen("aws logs describe-log-groups --region %s --profile %s" %(region, profile)).read()

log_groups_json = json.loads(output)

for log_group in log_groups_json["logGroups"]:
    if log_group_name in log_group["logGroupName"]:
        log_group_name = log_group["logGroupName"]
        break

log_stream_names = []

if log_stream_name is None:
    output = os.popen("aws logs describe-log-streams --region %s --profile %s --log-group-name %s --order-by LastEventTime --descending --max-items %s" %(region, profile, log_group_name, most_recent)).read()
    log_streams_json = json.loads(output)
    index = 0
    for log_stream in log_streams_json["logStreams"]:
        index += 1
        log_stream_name = log_stream["logStreamName"]
        log_stream_names.append(log_stream_name)
        if index == most_recent:
            break
else:
    log_stream_names.append(log_stream_name)

for log_stream_name in log_stream_names:
    output = os.popen("aws logs get-log-events --region %s --profile %s --log-group-name %s --log-stream-name %s --start-time %s --limit 10000 --start-from-head" %(region, profile, log_group_name, log_stream_name, start)).read()

    output_json = json.loads(output)
    next_token = output_json['nextForwardToken']
    events = output_json['events']

    print "\n\n=== %s ===\n\n" % log_stream_name

    if events is not None:
        for event in events:
            print "%s" % json.dumps(event, indent=4, separators=(',', ': '))

    while True:
        output = os.popen("aws logs get-log-events --region %s --profile %s --log-group-name %s --log-stream-name %s --limit 10000 --next-token %s --start-from-head" %(region, profile, log_group_name, log_stream_name, next_token)).read()

        output_json = json.loads(output)
        next_token = output_json['nextForwardToken']
        events = output_json['events']

        last = 0
        if events is None or len(events) == 0:
            break

        for event in events:
            print "%s" % json.dumps(event, indent=4, separators=(',', ': '))
            last = event['timestamp']

        if next_token is None:
            break

        if last > stop:
            break
