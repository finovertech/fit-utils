#!/usr/local/bin/python
# -*- coding: utf-8 -*-

#
# For factern operations like list, describe and history
#
#     python factern-ops.py --account-id 18BA0120-EB76-41A4-8F2C-857748A06812 --app-id 119C2386-546F-4A78-987F-69E5A58AC09C --app-secret abc -o describe D16C0F03-1E2B-44B0-AF65-F82606D7B379
#
# For describing node D16C0F03-1E2B-44B0-AF65-F82606D7B379
#

import argparse
import base64
from datetime import datetime
import httplib
import json
import logging
import os
import time
import urllib
import urllib2
import uuid

def pretty_json(jdata):
    return json.dumps(jdata, indent=4, sort_keys=True)


class FacternApi:
    """Factern client API implementation"""
    def __init__(self, account_id, app_id, application_secret, user_id, endpoint):
        self.account_id = account_id
        self.app_id = app_id
        self.application_secret = application_secret
        self.user_id = user_id
        self.client_token = None
        self.endpoint = endpoint

    def set_token(self):
        auth = base64.urlsafe_b64encode("%s:%s" %(self.app_id, self.application_secret))
        url = "https://sso.factern.com/auth/token"

        req = urllib2.Request(url=url, data="grant_type=client_credentials")
        req.add_header("Content-type", 'application/x-www-form-urlencoded')
        req.add_header("Accept", 'application/json')
        req.add_header("Authorization", 'Basic %s' %auth)

        resp = urllib2.urlopen(req)
        resp_data = resp.read()
        resp_json = json.loads(resp_data)

        self.client_token = resp_json['access_token']

    def call(self, path, request_json):
        url = "%s/%s?user=%s&operatingAs=%s" %(self.endpoint, path, self.user_id, self.user_id)

        json_str = json.dumps(request_json)

        req = urllib2.Request(url=url, data=json_str)
        req.add_header("Content-type", 'application/json')
        req.add_header("Accept", 'application/json')
        req.add_header("Authorization", 'Bearer %s' %self.client_token)

        resp = urllib2.urlopen(req)
        resp_data = resp.read()
        resp_json = json.loads(resp_data)
        return resp_json

    def read(self, path, request_json):
        url = "%s/%s?user=%s&operatingAs=%s" %(self.endpoint, path, self.user_id, self.user_id)

        json_str = json.dumps(request_json)

        req = urllib2.Request(url=url, data=json_str)
        req.add_header("Content-type", 'application/json')
        req.add_header("Accept", 'application/json')
        req.add_header("Authorization", 'Bearer %s' %self.client_token)

        resp = urllib2.urlopen(req)
        return resp.read()

    def create_entity(self, parent_id):
        req = {
            'nodeClass': 'Entity',
            'parentId': parent_id
        }
        return self.call('create', req)["nodeId"]

    def create_field_type(self, parent_id, name, description="None", data_type="string"):
        req = {
            "nodeClass": "FieldType",
            "parentId": parent_id,
            "name": name,
            "description": description,
            "dataType": data_type
        }
        return self.call('create', req)["nodeId"]

    def create_field(self, parent_id, field_type, value):
        req = {
            "nodeClass": "Field",
            "parentId": parent_id,
            "typeId": field_type,
            "data": value
        }
        return self.call('create', req)["nodeId"]

    def create_watch(self, parent_id):
        req = {
            "nodeClass": "Watch",
            "parentId": parent_id
        }
        return self.call('create', req)["nodeId"]

    def list_nodes(self, parent_id, node_class=None):
        req = {
            "nodeId": parent_id,
            "depth": 1
        }
        if node_class is not None:
            req['nodeClass'] = node_class
        else:
            req['nodeClass'] = "Entity"

        print "Request: %s" %req

        return self.call('list', req)["nodes"]

    def list_node_history(self, node_id):
        req = {
            "nodeId": node_id
        }
        return self.call('history', req)["nodes"]

    def describe_node(self, node_id):
        req = {
            "nodeId": node_id
        }
        return self.call('describe', req)

    def search(self, term, nodeClass):
        req = {
            "property": "name",
            "nodeClass": nodeClass,
            "operator": "equals",
            "term": term
        }
        return self.call('search', req)

    def replace(self, node_id, value):
        req = {
            "nodeId": node_id,
            "data": value
        }
        return self.call('replace', req)["nodeId"]

    def read_node(self, node_id):
        req = {
            "nodeId": node_id,
        }
        return self.read('read', req)

if __name__ == "__main__":
    FORMAT = "%(asctime)-15s %(message)s"
    logging.basicConfig(format=FORMAT, level=logging.DEBUG)

    desc = "Query factern"

    parser = argparse.ArgumentParser(description=desc)

    parser.add_argument("-a",
                        "--account-id",
                        required=True,
                        help="Guid of account id")

    parser.add_argument("-c",
                        "--app-id",
                        required=True,
                        help="Guid of application id")

    parser.add_argument("-u",
                        "--user-id",
                        help="Guid of user id")

    parser.add_argument("-s",
                        "--app-secret",
                        required=True,
                        help="Application secret")

    parser.add_argument("-o",
                        "--operation",
                        nargs='+',
                        help="operation + args ex -o describe EBE880F6-4DE3-45E1-BD41-5E82630D24C5")

    parser.add_argument("-e",
                        "--end-point",
                        required=True,
                        help="Factern endpoint")

    args = parser.parse_args()

    account_id = args.account_id or os.environ['ACCOUNT_ID']
    user_id = args.user_id or account_id
    app_id = args.app_id or os.environ['APPLICATION_ID']
    application_secret = args.app_secret or os.environ['APPLICATION_SECRET']
    endpoint = args.end_point or "https://api.factern.com/v1"

    ops = args.operation

    op = ops[0]

    try:
        start = time.time()
        factern = FacternApi(account_id, app_id, application_secret, user_id, endpoint)

        factern.set_token()

        if op == "describe":
            print "%s" %pretty_json(factern.describe_node(ops[1]))

        if op == "list":
            print "%s" %pretty_json(factern.list_nodes(ops[1]))

        if op == "history":
            print "%s" %pretty_json(factern.list_node_history(ops[1]))

        if op == "search":
            print "%s" %pretty_json(factern.search(ops[1], ops[2]))

        end = time.time()
        elapsed_millis = int((end - start) * 1000)
        logging.info("Elapsed: took %d", elapsed_millis)
    except Exception as e:
        logging.warn('Operation failed', e)
        raise
