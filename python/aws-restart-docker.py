#!/usr/local/bin

#
# For restarting docker verticles. Ex.
#
#     python aws-restart-docker.py Factern-Metadata --profile prod --region eu-west-1
#

import argparse
from datetime import datetime
import json
import os

desc = "Download logs from CloudWatch"

parser = argparse.ArgumentParser(description=desc)

parser.add_argument("name",
                    metavar="name",
                    nargs='+',
                    help="Ex. Factern-Metadata")

parser.add_argument("-p",
                    "--profile",
                    help="Aws profile ex. prod")

parser.add_argument("-i",
                    "--identity-profile",
                    help="Location of your pem file")

parser.add_argument("-r",
                    "--region",
                    help="Aws region ex. eu-west-1")

args = parser.parse_args()

if not len(args.name) == 1:
    raise Exception("At this time we only support restarting a single verticle stack")

ec2_instances_name = args.name[0]
profile = args.profile or "dev"
region = args.region or "us-east-1"
identity_profile = args.identity_profile or "~/.ssh/factern-prod-ssh.pem"

output = os.popen("aws ec2 describe-instances --filters Name=tag:Name,Values=%s --profile %s --region %s" %(ec2_instances_name, profile, region)).read()

output_json = json.loads(output)

# Likely does not work for spot instances
reservations = output_json['Reservations']

if reservations is None or len(reservations) < 1:
    raise Exception("Could not find any instances")

ips = []
for reservation in reservations:
    instances = reservation["Instances"]
    if instances is None:
        continue
    for instance in instances:
        ip = instance["PublicIpAddress"]
        if ip is None:
            continue
        ips.append(ip)

if len(ips) < 1:
    raise Exception("Could not find any instances")

for ip in ips:
    print "IP: %s" %ip

    output = os.popen('ssh -y -i %s ec2-user@%s "docker ps | grep factern"' %(identity_profile, ip)).read()

    container_id = output.split()[0]
    print "Container id %s" %container_id

    output = os.popen('ssh -y -i %s ec2-user@%s "docker restart %s"' %(identity_profile, ip, container_id)).read()

    print "Restart output %s" %output
