#!/usr/local/bin/python

import argparse
import binascii
import os
import struct

if __name__ == "__main__":
    desc = "Generate a factern uuid"

    parser = argparse.ArgumentParser(description=desc)

    parser.add_argument("-d",
                        "--domain",
                        help="Domain id of the uuid")

    args = parser.parse_args()

    in_domain = args.domain or "0"

    domain_bits = struct.pack(">I", int(in_domain))

    random_bits = os.urandom(20)

    fuuid_bits = domain_bits + random_bits

    print "%s" % binascii.hexlify(fuuid_bits).upper()
