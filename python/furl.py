#!/usr/local/bin/python
# -*- coding: utf-8 -*-

#
# For interacting with factern data service
#
#     python furl.py --account-id 18BA0120-EB76-41A4-8F2C-857748A06812 --app-id 119C2386-546F-4A78-987F-69E5A58AC09C --app-secret abc --method create --payload "{\"nodeClass\": \"Entity\", \"parentId\": \"22FAF059-93DB-43B4-9C92-D11097F0A278\"}"
#

import argparse
import base64
from datetime import datetime
import httplib
import json
import logging
import os
import time
import urllib
import urllib2
import uuid

def str2bool(v):
  return v.lower() in ("yes", "true", "t", "1")

class FacternApi:
    """Factern client API implementation"""
    def __init__(self, account_id, app_id, application_secret, user_id, version):
        self.account_id = account_id
        self.app_id = app_id
        self.application_secret = application_secret
        self.user_id = user_id
        self.client_token = None
        self.version = version

    def set_token(self):
        auth = base64.urlsafe_b64encode("%s:%s" %(self.app_id, self.application_secret))
        url = "https://sso.factern.com/auth/token"

        req = urllib2.Request(url=url, data="grant_type=client_credentials")
        req.add_header("Content-type", 'application/x-www-form-urlencoded')
        req.add_header("Accept", 'application/json')
        req.add_header("Authorization", 'Basic %s' %auth)


        resp = urllib2.urlopen(req)
        resp_data = resp.read()
        resp_json = json.loads(resp_data)

        self.client_token = resp_json['access_token']

    def call(self, path, request_json):
        url = "https://api.factern.com/%s/%s?user=%s&operatingAs=%s" %(self.version, path, self.user_id, self.user_id)

        json_str = json.dumps(request_json)

        req = urllib2.Request(url=url, data=json_str)
        req.add_header("Content-type", 'application/json')
        req.add_header("Accept", 'application/json')
        req.add_header("Authorization", 'Bearer %s' %self.client_token)

        resp = urllib2.urlopen(req)
        resp_data = resp.read()
        resp_json = json.loads(resp_data)
        return json.dumps(resp_json, indent=4, separators=(',', ': '))

if __name__ == "__main__":
    FORMAT = "%(asctime)-15s %(message)s"
    logging.basicConfig(format=FORMAT, level=logging.DEBUG)

    desc = "Monitor watch event generation"

    parser = argparse.ArgumentParser(description=desc)

    parser.add_argument("-a",
                        "--account-id",
                        required=True,
                        help="Guid of account id")

    parser.add_argument("-c",
                        "--app-id",
                        required=True,
                        help="Guid of application id")

    parser.add_argument("-u",
                        "--user-id",
                        help="Guid of user id")

    parser.add_argument("-v",
                        "--version",
                        help="API version")

    parser.add_argument("-s",
                        "--app-secret",
                        required=True,
                        help="Application secret")

    parser.add_argument("-m",
                        "--method",
                        required=True,
                        help="Method")

    parser.add_argument("-p",
                        "--payload",
                        help="Payload")

    args = parser.parse_args()

    account_id = args.account_id or os.environ['ACCOUNT_ID']
    user_id = args.user_id or account_id
    app_id = args.app_id or os.environ['APPLICATION_ID']
    application_secret = args.app_secret or os.environ['APPLICATION_SECRET']
    version = args.version or "v2"

    method = args.method
    payload = json.loads(args.payload or "{}")

    try:
        start = time.time()
        factern = FacternApi(account_id, app_id, application_secret, user_id, version)

        factern.set_token()

        print "%s" % factern.call(method, payload)

        end = time.time()
        elapsed_millis = int((end - start) * 1000)
        logging.info("Elapsed: took %d", elapsed_millis)
    except Exception as e:
        logging.warn('Check failed', e)
        raise
