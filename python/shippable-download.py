#!/usr/local/bin/python
# -*- coding: utf-8 -*-

import argparse
import base64
from datetime import datetime
import httplib
import json
import logging
import os
import time
import urllib
import urllib2

shippable_api_key = '5bac2b1d-8a22-418f-a633-fb207f3f588d'

def get_job_output(job_id):
    url = "https://api.shippable.com/jobs/%s/consoles?download=true" %job_id

    print "%s" % url

    req = urllib2.Request(url=url)
    req.add_header("Authorization", 'apiToken %s' %shippable_api_key)
    req.add_header("Accept", 'text/plain')

    resp = urllib2.urlopen(req)
    return resp.read()

def list_jobs():
    url = "https://api.shippable.com/jobs"

    req = urllib2.Request(url=url)
    req.add_header("Authorization", 'apiToken %s' %shippable_api_key)
    req.add_header("Accept", 'text/plain')

    resp = urllib2.urlopen(req)

    resp_data = resp.read()
    resp_json = json.loads(resp_data)
    return json.dumps(resp_json, indent=4, separators=(',', ': '))

if __name__ == "__main__":
    FORMAT = "%(asctime)-15s %(message)s"
    logging.basicConfig(format=FORMAT, level=logging.DEBUG)

    desc = "Work with shippable API"

    parser = argparse.ArgumentParser(description=desc)

    parser.add_argument("-j",
                        "--job-id",
                        help="Job id")

    parser.add_argument("-l",
                        "--list-jobs",
                        action='store_true',
                        help="List jobs")

    args = parser.parse_args()

    do_list_jobs = args.list_jobs or False
    job_id = args.job_id or None

    try:
        start = time.time()
        if do_list_jobs:
            print "%s" %list_jobs()
        else:
            print "%s" %get_job_output(job_id)
        end = time.time()
        elapsed_millis = int((end - start) * 1000)
        logging.info("Elapsed: took %d", elapsed_millis)
    except Exception as e:
        logging.warn('Check failed', e)
        raise
