#!/usr/bin/env python

import argparse
import os

desc = "Download load test reports from S3 bucket factern-load-test."

parser = argparse.ArgumentParser(description=desc)

parser.add_argument("-m",
                    "--most-recent",
                    help="Number of most recent reports to download. Defaults to most recent report")

args = parser.parse_args()

most_recent = int(args.most_recent or 1)

profile = "dev"

region = "us-east-1"

output = os.popen(
    "aws s3 ls s3://factern-load-test --profile dev | awk '{print $2}' | sort -r | head -n %s" % most_recent).read()

folders = output.split("\n")

for folder in folders[:-1]:
    print "aws s3 sync s3://factern-load-test/%s ./load-test-reports/%s" % (folder, folder)
    os.popen("aws s3 sync s3://factern-load-test/%s ./load-test-reports/%s" % (folder, folder))
