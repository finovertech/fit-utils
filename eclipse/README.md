# Eclipse configuration files

## Java Directory
Contains code styling and formatting configuration for Eclipse.
Within Eclipse go to Window->Preferences->Java->CodeStyle then for each section there is a file you can import the appropriate file and make it the active config.
