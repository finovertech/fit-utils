@Grapes([
    @Grab('com.github.groovy-wslite:groovy-wslite:1.1.3')
])
import wslite.rest.ContentType
import wslite.rest.RESTClient
import static groovy.json.JsonOutput.toJson
import static groovy.json.JsonOutput.prettyPrint

/**
Summary: describe a Node in Factern
Run: groovy describeFacternNode.groovy {-argName argValue}...

Required args:
- jwt
- id
*/

final String master = '2a28f979-0bf4-4452-a8af-3c8096246f29' // THIS MIGHT CHANGE!!

////////

final Map opts = [:]
args.toList().eachWithIndex { a, i ->
    if (!(i%2)) {
        opts[(args[i] - '-').toLowerCase()] = args[i + 1]
    }
}

println opts

if (!opts.jwt
    || !opts.id
) {
    println "ABORT:: Invalid args: ${opts}; required: -jwt, -id"
    return
}

String authToken = opts.jwt // eg. eyJraWQiOiJyc2ExIiwiYWxnIjC9hdXR....Zjkjdv1p2Grg8joaoe6B2kFQSbYHg

def client = new RESTClient()
client.setUrl("https://api.factern.com/v1/describe?user=${master}&operatingAs=${master}")

final Map body = [
    nodeId: opts.id,
]

println toJson(body)

try {
    def resp = client.post([headers:["Authorization": "Bearer ${authToken}"]], {
                type ContentType.JSON
                json body
            })
    println prettyPrint(toJson(resp.json))

} catch (Exception ex) {
    println ex.message
}
