@Grapes([
    @Grab('com.github.groovy-wslite:groovy-wslite:1.1.3')
])
import wslite.rest.ContentType
import wslite.rest.RESTClient
import static groovy.json.JsonOutput.toJson
import static groovy.json.JsonOutput.prettyPrint

/**
Summary: create an Account or App in Factern
Run: groovy createFacternAccount.groovy {-argName argValue}...

Required args:
- jwt
- type ('account' || 'app')
- accountid (if type == 'app')
*/

final String master = '2a28f979-0bf4-4452-a8af-3c8096246f29' // THIS MIGHT CHANGE!!

////////

final Map opts = [:]
args.toList().eachWithIndex { a, i ->
    if (!(i%2)) {
        opts[(args[i] - '-').toLowerCase()] = args[i + 1]
    }
}

println opts

if (!opts.jwt
    || !opts.type
    || (opts.type == 'app' && !(opts.accountid ?: opts.parentid))
) {
    println "ABORT:: Invalid args: ${opts}; required: -jwt, -type {-accountid}"
    return
}

String authToken = opts.jwt // eg. eyJraWQiOiJyc2ExIiwiYWxnIjC9hdXR....Zjkjdv1p2Grg8joaoe6B2kFQSbYHg

def client = new RESTClient()
client.setUrl("https://api.factern.com/v1/create?user=${master}&operatingAs=${master}")

final Map body
if (opts.type == 'account') {
    client.setUrl("https://api.factern.com/v1/create?user=${master}&operatingAs=${master}")

    body = [
        nodeClass: 'Account',
        parentId: opts.parentid ?: master,
        givenName: opts.givenname ?: 'Bob',
        familyName: opts.familyname ?: 'Loblaw',
        email: opts.email ?: 'boblaw_'+Math.random()+'@factern.com',
        password: opts.password ?: 'supersecret',
        phoneNumber: opts.phonenumber ?: '555-555-5555',
    ]

} else if (opts.type == 'app') {
    client.setUrl("https://api.factern.com/v1/create?user=${opts.accountid ?: opts.parentid}&operatingAs=${opts.accountid ?: opts.parentid}")

    body = [
        nodeClass: 'Application',
        parentId: opts.accountid ?: opts.parentid,
        name: opts.name ?: "App_${Math.random()}",
        redirectUris: [opts.redirecturi ?: 'http://localhost:8080']
    ]

} else {
    println "ABORT:: Unknown type: ${opts.type}"
    return
}

println toJson(body)

try {
    def resp = client.post([headers:["Authorization": "Bearer ${authToken}"]], {
                type ContentType.JSON
                json body
            })
    println prettyPrint(toJson(resp.json))

} catch (Exception ex) {
    println ex.message
}
