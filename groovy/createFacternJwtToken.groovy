@Grapes([
    @Grab('com.github.groovy-wslite:groovy-wslite:1.1.3')
])
import wslite.rest.ContentType
import wslite.rest.RESTClient
import static groovy.json.JsonOutput.toJson
import static groovy.json.JsonOutput.prettyPrint
import java.util.Base64

/**
Summary: create an token from client credentials
Run: groovy createFacternJwtToken.groovy {-argName argValue}...

Required args:
- clientid
- secret
*/

final Map opts = [:]
args.toList().eachWithIndex { a, i ->
    if (!(i%2)) {
        opts[(args[i] - '-').toLowerCase()] = args[i + 1]
    }
}

if (!opts.clientid
    || !opts.secret
) {
    println "ABORT:: Invalid args: ${opts}; required: -clientid, -secret"
    return
}

String clientId = opts.clientid // eg. 'Z28dbc63-97e2-4f02-bf12-3450d97ab720'
String secret = opts.secret // eg. 'ZaQokFoNh8Dp4c2rkDdYcexi9H6Gv4tHI4SuxzBoAMSv+WuEX98/x+3xYJV60Ikvlui1C2s3n68nM0D4fQWCLpu5z6X8kFHMIFZBsnceczsTdxLPmBl+R2ByyEttNHi2KPbejLr4R7mKSXM78jgRjlqUp5V/xrpTV/YvnoGBs48='
String authorization = Base64.encoder.encodeToString("${clientId}:${secret}".bytes as byte[])

def client = new RESTClient()
client.setUrl('https://sso.factern.com/auth/token')

try {
    def resp = client.post(
        [headers:["Authorization": "Basic ${authorization}"]], {
                type 'application/x-www-form-urlencoded'
                text 'grant_type=client_credentials'
            })
    println prettyPrint(toJson(resp.json))

} catch (Exception ex) {
    println "ERROR:" + ex.message
}
