# Java

## Code Coverage
After some testing of Jacoco and Cobertura, we have decided to use *Jacoco* as the standard tooling. (Biggest reason was when using Cobertura, some source files were being missed in coverage and exclusions were not working on subprojects.)

Below is the gradle config for *Jacoco* in your Gradle project:

(Note: this example assumes a single project, using `plugins {...}` and no `subprojects {...}`)

```groovy
buildscript {
    repositories {
        jcenter()
    }

    dependencies {
        classpath 'com.google.guava:guava:19.0'
    }
}

plugins {
    id 'jacoco'
    id 'com.palantir.jacoco-coverage' version '0.3.0-dirty' //https://github.com/palantir/gradle-jacoco-coverage
    id 'com.palantir.jacoco-full-report' version '0.3.0-dirty' //https://github.com/palantir/gradle-jacoco-coverage
}

test {
    jacoco {
        //http://eclemma.org/jacoco/trunk/doc/agent.html
        append = false
    }
}

test.finalizedBy jacocoTestReport

jacoco {
    toolVersion = '0.7.6.201602180812'
}

jacocoTestReport {
    reports {
        xml.enabled true
        csv.enabled true
    }

    afterEvaluate {
        classDirectories = files(classDirectories.files.collect {
            fileTree(dir: it, exclude: [
                    'com/some/path/MyClass.class'
            ])
        })
    }
}

jacocoCoverage {
    //http://eclemma.org/jacoco/trunk/doc/counters.html
    fileThreshold 0.9, LINE
    fileThreshold 0.6, BRANCH
    fileThreshold 1.0, METHOD
    fileThreshold 1.0, CLASS

    packageThreshold 0.8, LINE
    packageThreshold 0.6, BRANCH

    reportThreshold 0.9, LINE
    reportThreshold 0.6, BRANCH

    fileThreshold 0.0, ~".*Test.java"
}
```

## Linting

Following linters have agreed upon rules stored here.

- checkstyle
- PMD
- codenarc

### Configuring Linting in Gradle:

We use gradle plugin [ru.vyarus.quality](https://github.com/xvik/gradle-quality-plugin):

(Note: this example assumes a multi project, using `subprojects {...}` and no `plugins {...}`)

In the `settings.gradle` file put:
```groovy
try {
    File checkstyleFile = new File("$rootDir/gradle/config/checkstyle/checkstyle.xml")
    checkstyleFile.getParentFile().mkdirs()
    checkstyleFile.withOutputStream { out ->
        new URL('https://bitbucket.org/finovertech/fit-utils/raw/master/java/checkstyle-config.xml').withInputStream { from ->
            out << from
        }
    }

    File codenarcFile = new File("$rootDir/gradle/config/codenarc/codenarc.xml")
    println 'codenarc'
    codenarcFile.getParentFile().mkdirs()
    codenarcFile.withOutputStream { out ->
        new URL('https://bitbucket.org/finovertech/fit-utils/raw/master/java/codenarc-config.xml').withInputStream { from ->
            out << from
        }
    }
} catch (Exception ignore) {
    println '! ERROR downloading code quality configs !'
}
```

In `build.gradle` file put:
```groovy
buildscript {
    repositories {
        jcenter()
    }
    dependencies {
        classpath 'com.amazonaws:aws-java-sdk:1.10.61'
        classpath 'ru.vyarus:gradle-quality-plugin:1.3.0'
    }
}

subprojects {
    apply plugin: 'ru.vyarus.quality' //https://github.com/xvik/gradle-quality-plugin#configuration

    quality {
        checkstyleVersion = '6.17'
        pmdVersion = '5.4.1'
        findbugsVersion = '3.0.1'
        codenarcVersion = '0.25'
        animalsnifferVersion

        pmd = false //we don't use PMD
        configDir = "$rootDir/gradle/config/"
    }
}
```

See [clive-web-service](https://bitbucket.org/finovertech/clive-web-service) for example of usage.
